# szew/fi

Index and compare file trees.

[![szew/io](https://clojars.org/szew/fi/latest-version.svg)](https://clojars.org/szew/fi)

[API Codox][latest], [CHANGELOG][changelog]

## Why

I've been dogfooding my private Clo*j*ure toolbox (named `szew`) since 2012.

Splitting out and releasing non-proprietary parts.

## What

It seems that comparing file trees is hard in bare Windows. Especially when
you're not allowed to install anything or copy those files out anywhere.  This
app is the 80/20 solution. It indexes and compares file trees, using paths
relative to point of entry as file identities. It walks a file tree and stores
results in a embedded [H2][h2database] instance, marking them with a common
"tag". File can be stored for diffing or hashed, both options are configurable.

Resulting File Index does this in three ways:

* matches (tag v. tag) -- hash/checksum comparison list (think BLOBs).
* manifest (tag v. tag) -- diff based list, with settable advisor.
* diff (tag v. tag) -- single HTML page showing diffs.

That's it.

**It's really basic, if you can use anything else -- do so.**

**Also: If you try to diff a bunch of binaries with this you'll gonna have
a bad time! Do matches only, select your `:hash-bytes` wisely!**

# Usage

All commands return something `deref`able, multiple harvests can run at the
same time.

Build `uberjar` and name it `fi.jar`. Run with `-jar` or put in classpath
and execute `szew.repl` namespace for `-main` action. Run like:

```bash
java -jar fi.jar script1.clj script2.clj
```

## Script Example

Compare contents of directories `A` and `B`.

```clojure
(in-ns 'user)
(require '[szew.fi :as fi])
(require '[szew.fi.entry :as entry])
(require '[szew.io :as io])
(require '[szew.repl :refer [set-log-level!]])
(require '[clojure.pprint :refer [print-table])

; Uncomment to see what's happening:
;(set-log-level! "szew.fi.index" :debug)
;(set-log-level! "szew.fi" :debug)

(def top-A (entry/tag-top "A" "datasets/tree_root/A"))

(def top-B (entry/tag-top "B" "datasets/tree_root/B"))

(defn go! []
  (fi/prep! "/tmp/xxx")
  (fi/init!))

(defn done! []
  @(fi/erase!)
  (fi/halt!))

(defn do-it!
  "Just some harvesting.
  "
  []
  @(fi/delete! "A")
  @(fi/delete! "B")
  (let [jobs (->> [(fi/store! (entry/gather top-A))
                   (fi/store! (entry/gather top-B))]
                  (mapv deref))
        ms   (io/sink (io/tsv) "dev-datasets/A_B_matches.txt")
        mt   (io/sink (io/tsv) "dev-datasets/A_B_manifest.txt")
        html (partial spit "dev-datasets/A_B_diffs.html")]
    (print-table @(fi/summary!))
    (ms @(fi/matches! "A" "B"))
    (mt @(fi/manifest! "A" "B"))
    (html @(fi/html-diffs! "A" "B"))))
```

Loading this will get you in a REPL. Then just run `(go!)` and `(do-it!)`.
Cleanup with `(done!)`.

## Custom manifest advisors

You can use custom advisor with `fi/manifest!`, default is:

```clojure
;; szew.fi.index/super-good-advice

(defn super-good-advice
  "Default advisor! Get entry-diff, give Super Good Advice(TM).
  "
  [entry-diff]
  (cond (and (= (str (:source_tag entry-diff) "+" (:target_tag entry-diff))
                (:presence entry-diff))
             (empty? (:changed entry-diff)))
        "IGNORE (Reason: No diff)"
        (= (:source_tag entry-diff) (:presence entry-diff))
        "DELETE (Reason: Not in target)"
        (= (:target_tag entry-diff) (:presence entry-diff))
        "REVIEW (Reason: Only in target)"
        :else
        "REVIEW (Reason: Diff in content)"))
```

You can write and plug in your own advisor, it can even reuse the provided
one for the already implemented cases.

What's that `entry-diff` that `super-good-advice` is getting fed?

```clojure
{:changed     [line-maps]
 :deltas      [delta-maps]
 :unidiff     [String]
 :name        String
 :presence    String   ;; tag1, tag2, tag1+tag2
 :rel_path    String
 :source_hash String | nil
 :source_tag  String
 :source_time timestamp | nil
 :target_hash String | nil
 :target_tag  String
 :target_time timestamp | nil}
```

To be precise:

* `:changed` is produced by `diff/line-maps` (skips `:tag` = `:equal`)
* `:deltas` is produced by `diff/delta-maps`
* `:unidiff` is produced by `diff/unidiff`

## Logging

This library uses `org.clojure/tools.logging` on top of `logback`.

## License

Copyright © Sławek Gwizdowski

MIT License, text can be found in the LICENSE file.

[latest]: https://spottr.bitbucket.io/szew-fi/latest/
[changelog]: CHANGELOG.md
[h2database]: https://h2database.com
