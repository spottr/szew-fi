; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Diffing utilities of last resort - tests."}
 szew.fi.diff-test
 (:require
   [szew.fi.diff
    :refer [unidiff ->Patch line-maps ->Deltas Delta->map delta-maps]]
   [clojure.test :refer [deftest testing is]]))

(def original ["A" "B" "C" "D" "E" "F"])

(def revised ["A" "B" "D" "E" "X"])

(def no-lines [])

(def nil-lines nil)

(deftest Simple-checks-for-simple-people-test

  (testing "Thin-wrappers over Patch and generateUnifiedDiff"
    (is (= [] (unidiff "no" "nil" no-lines (->Patch no-lines nil-lines) 0)))
    (is (= ["--- 1"
            "+++ 2"
            "@@ -3,1 +3,0 @@"
            "-C"
            "@@ -6,1 +5,1 @@"
            "-F"
            "+X"]
           (unidiff "1" "2" original (->Patch original revised) 0)))
    (is (= ["--- 1"
            "+++ 2"
            "@@ -2,5 +2,4 @@"
            " B"
            "-C"
            " D"
            " E"
            "-F"
            "+X"]
           (unidiff "1" "2" original (->Patch original revised) 1)))
    (is (= ["--- 1"
            "+++ 2"
            "@@ -1,6 +1,5 @@"
            " A"
            " B"
            "-C"
            " D"
            " E"
            "-F"
            "+X"]
           (unidiff "1" "2" original (->Patch original revised) 3))))

  (testing "Maps of lines, via line-maps"
    (is (= [] (line-maps no-lines no-lines)))
    (is (= [] (line-maps nil-lines nil-lines)))
    (is (= [{:tag :equal :old "a" :at 1}]
           (line-maps ["a"] ["a"])))
    (is (= [{:tag :equal :old "A" :at 1}
            {:tag :equal :old "B" :at 2}
            {:tag :delete :old "C" :at 3}
            {:tag :equal :old "D" :at 4}
            {:tag :equal :old "E" :at 5}
            {:tag :change :old "F" :new "X" :at 6}]
           (line-maps original revised)))
    (is (= [{:tag :delete :old "A" :at 1}
            {:tag :delete :old "B" :at 2}
            {:tag :delete :old "D" :at 3}
            {:tag :delete :old "E" :at 4}
            {:tag :delete :old "X" :at 5}]
           (line-maps revised no-lines)))
    (is (= [{:tag :insert :new "A" :at 1}
            {:tag :insert :new "B" :at 2}
            {:tag :insert :new "D" :at 3}
            {:tag :insert :new "E" :at 4}
            {:tag :insert :new "X" :at 5}]
           (line-maps no-lines revised)))
    (is (= [{:tag :delete :old "A" :at 1}
            {:tag :delete :old "B" :at 2}
            {:tag :delete :old "D" :at 3}
            {:tag :delete :old "E" :at 4}
            {:tag :delete :old "X" :at 5}]
           (line-maps revised no-lines)))
    (is (= (line-maps revised nil-lines)
           (line-maps revised no-lines))))

  (testing "Deltas and Chunks"
    (is (= [] (->Deltas no-lines no-lines)))
    (is (= [] (->Deltas nil-lines nil-lines)))
    (is (= 2 (count (->Deltas original revised))))
    (is (= {:type     :delete
            :original {:at   2
                       :span 1
                       :lines ["C"]}}
          (Delta->map (first (->Deltas original revised)))))
    (is (= {:type     :change
            :original {:at    5
                       :span  1
                       :lines ["F"]}
            :revised  {:at    4
                       :span  1
                       :lines ["X"]}}
          (Delta->map (last (->Deltas original revised))))))

  (testing "Maps of deltas, via delta-maps"
    (is (= [] (delta-maps no-lines no-lines)))
    (is (= [] (delta-maps nil-lines nil-lines)))
    (is (= [] (delta-maps ["a"] ["a"])))
    (is (= [{:type     :delete
             :original {:at    2
                        :span  1
                        :lines ["C"]}}
            {:type     :change
             :original {:at    5
                        :span  1
                        :lines ["F"]}
             :revised  {:at    4
                        :span  1
                        :lines ["X"]}}]
           (delta-maps original revised)))
    (is (= [{:type :delete :original {:at 0 :span 5 :lines revised}}]
           (delta-maps revised no-lines)))
    (is (= (delta-maps revised nil-lines)
           (delta-maps revised no-lines)))))
