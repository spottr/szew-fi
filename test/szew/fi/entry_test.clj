; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Files into index entries - tests."}
 szew.fi.entry-test
 (:require
   [szew.fi.entry :refer [entry tag-top gather]]
   [szew.io :as io]
   [clojure.java.io :as clj.io]
   [clojure.test :refer [deftest testing is]]))

(deftest File-to-entry-test
  (let [spec (entry {:top-path    "datasets/tree_root/A/sub"
                     :tag         "A"
                     :hash-bytes  :skip
                     :store-up-to :skip})
        abs  (.getCanonicalPath (clj.io/as-file (:top-path spec)))
        path "datasets/tree_root/A/sub/A_only.log"]

    (testing "No storage, no hashing."
      (let [data (io/in! spec path)]
        (is (= abs (:top_path data)))
        (is (= (:tag data) (:tag spec)))
        (is (= (:name data) "A_only.log"))
        (is (= (:stored_b data) 0))
        (is (= (:hashed_b data) 0))
        (is (nil? (:short_hash data)))
        (is (nil? (:content data)))))

    (testing "No storage, full hashing."
      (let [data (io/in! (assoc spec :hash-bytes :full) path)]
        (is (= abs (:top_path data)))
        (is (= (:tag data) (:tag spec)))
        (is (= (:name data) "A_only.log"))
        (is (= (:stored_b data) 0))
        (is (= (:hashed_b data) (:size data)))
        (is (= (:short_hash data)
               (str "930f538105a251f6f979752499ffdb93daac243ea80e94cf81473b4"
                    "96ebdb24e")))
        (is (nil? (:content data)))))

    (testing "Full storage, no hashing."
      (let [data (io/in! (assoc spec :store-up-to :full) path)]
        (is (= abs (:top_path data)))
        (is (= (:tag data) (:tag spec)))
        (is (= (:name data) "A_only.log"))
        (is (= (:stored_b data) (:size data)))
        (is (= (:hashed_b data) 0))
        (is (nil? (:short_hash data)))
        (is (= (:content data) "A_only.log\n"))))

    (testing "Full storage, full hashing."
      (let [spec+ (assoc spec :store-up-to :full :hash-bytes :full)
            data  (io/in! spec+ path)]
        (is (= abs (:top_path data)))
        (is (= (:tag data) (:tag spec)))
        (is (= (:name data) "A_only.log"))
        (is (= (:stored_b data) (:size data)))
        (is (= (:hashed_b data) (:size data)))
        (is (= (:short_hash data)
               (str "930f538105a251f6f979752499ffdb93daac243ea80e94cf81473b4"
                    "96ebdb24e")))
        (is (= (:content data) "A_only.log\n"))))

    (testing "Some storage (below size nada), some hashing."
      (let [spec+ (assoc spec :store-up-to 1 :hash-bytes 1)
            data  (io/in! spec+ path)]
        (is (= abs (:top_path data)))
        (is (= (:tag data) (:tag spec)))
        (is (= (:name data) "A_only.log"))
        (is (= (:stored_b data) 0))
        (is (= (:hashed_b data) 1))
        (is (= (:short_hash data)
               (str "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a8"
                    "8a08fdffd")))
        (is (nil? (:content data)))))

    (testing "Some storage (above size stored), some hashing."
      (let [spec+ (assoc spec :store-up-to 16 :hash-bytes 1)
            data  (io/in! spec+ path)]
        (is (= abs (:top_path data)))
        (is (= (:tag data) (:tag spec)))
        (is (= (:name data) "A_only.log"))
        (is (= (:stored_b data) (:size data)))
        (is (= (:hashed_b data) 1))
        (is (= (:short_hash data)
               (str "559aead08264d5795d3909718cdd05abd49572e84fe55590eef31a8"
                    "8a08fdffd")))
        (is (= (:content data) "A_only.log\n"))))

    (testing "Malformed spec, shout exceptions."
      (is (thrown? Exception (io/in! (dissoc spec :top-path) path)))
      (is (thrown? Exception (io/in! (dissoc spec :tag) path)))
      (is (thrown? Exception (io/in! (assoc spec :hash-bytes :wat?) path)))
      (is (thrown? Exception (io/in! (assoc spec :store-up-to :wat?) path)))
      ;(is (thrown? Exception (io/in! (assoc spec :hash-bytes 0) path)))
      ;(is (thrown? Exception (io/in! (assoc spec :store-up-to 0) path)))
      (is (thrown? Exception (io/in! (assoc spec :hash-bytes -1) path)))
      (is (thrown? Exception (io/in! (assoc spec :store-up-to -1) path)))
      (is (thrown? Exception (io/in! (assoc spec :encoding :fu) path))))))

(deftest Gather-entries-testing

  (testing "Basic index gathering."
    (let [top-A (tag-top "A" "datasets/tree_root/A")
          abs-A (.getCanonicalPath (clj.io/as-file "datasets/tree_root/A"))
          top-B (tag-top "B" "datasets/tree_root/B")
          abs-B (.getCanonicalPath (clj.io/as-file "datasets/tree_root/B"))]
      (is (= (assoc (entry) :tag "A" :top-path abs-A) top-A))
      (is (= (assoc (entry) :tag "B" :top-path abs-B) top-B))
      (is (= 6 (count (gather top-A))))
      (is (= 6 (count (gather top-B))))
      (is (= 121
             (reduce + (map :size (concat (gather top-A) (gather top-B))))))
      (is (= #{"1.log" "1.txt" "A_only.log" "A_only.txt" "B_only.txt"}
             (set (map :name (concat (gather top-A) (gather top-B)))))))))
