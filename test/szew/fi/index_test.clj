; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Cold storage - tests."}
 szew.fi.index-test
 (:require
   [szew.fi.entry :refer [tag-top gather]]
   [szew.fi.index
    :refer [summary seed! store-batched! delete-tag! list-index-by-tag
            tag-match tag-match-manifest tag-diff tag-diff-manifest
            fetch-entry entry-diff]]
   [szew.h2 :as h2]
   [szew.h2.java-jdbc :refer [raze!]]
   [clojure.test :refer [deftest testing is]]))

;;
;; Directory tree:
;;
;; dir: tree_root
;;   dir: A
;;     file: 1.log
;;     file: 1.txt
;;     file: A_only.txt
;;     dir: sub
;;       file: 1.log
;;       file: 1.txt
;;       file: A_only.log
;;   dir: B
;;     file: 1.log
;;     file: 1.txt
;;     file: B_only.txt
;;     dir: sub
;;       file: 1.log
;;       file: 1.txt
;;       file: B_only.log
;;

(defn file-please!
  "Gimme a temporary File.
  "
  []
  (io! "Mr Java.Io.File, bring me a File!"
    (doto (java.io.File/createTempFile "szew-fi-test-" ".bin")
      (.deleteOnExit))))

(defn thanks-for-the-file!
  "Delete a File.
  "
  [^java.io.File f]
  (when f
    (.delete f)))

(deftest Testing-basic-index

  (let [tmp (file-please!)
        db  {:datasource
             (-> (.getCanonicalPath ^java.io.File tmp)
                (h2/spec)
                (h2/spec->DS))}
        t-A (tag-top "A" "datasets/tree_root/A")
        t-B (tag-top "B" "datasets/tree_root/B")]

    (try

      (testing "Seeding and all."

        (is (thrown? Exception (summary db)))
        (seed! db)
        (is (empty? (summary db)))
        (is (= 6 (store-batched! db (gather t-A))))
        (is (= #{{:tag "A" :count 6}} (set (summary db))))
        (is (= 6 (store-batched! db (gather t-B))))
        (is (= #{{:tag "A" :count 6} {:tag "B" :count 6}} (set (summary db))))
        (is (= 6 (delete-tag! db "A")))
        (is (= #{{:tag "B" :count 6}} (set (summary db))))
        (is (= 6 (store-batched! db (gather t-A))))
        (is (= #{{:tag "A" :count 6} {:tag "B" :count 6}} (set (summary db)))))

      (testing "Fetching stored entries."

        (is (= (set (map #(dissoc % :fetched_at :content :write_time)
                         (gather t-A)))
               (set (map #(dissoc % :fetched_at :write_time)
                         (list-index-by-tag db {:tag "A"})))))
        (is (= (set (map #(dissoc % :fetched_at :content :write_time)
                         (gather t-B)))
               (set (map #(dissoc % :fetched_at :write_time)
                         (list-index-by-tag db {:tag "B"}))))))

      (testing "Big match map."

        (let [match-map (select-keys (tag-match db "A" "B")
                                     ["/sub/1.txt" "/1.log" "/A_only.txt"])
              shortcut  (->> match-map
                             (tag-match-manifest)
                             (drop 1)
                             (map (juxt first #(get % 2)))
                             (into {}))]
          (is (= {"/1.log"      "false"
                  "/sub/1.txt"  "true"
                  "/A_only.txt" "true"}
                 shortcut))
          (is (= {:name "1.log"
                  :presence "A+B"
                  :rel_path "/1.log"
                  :source_hash (str "443e922e685ab5b904af6ed5f897480e318d1fbc3"
                                    "e158b9092b18fcaf24f7c7b")
                  :source_tag "A"
                  :target_hash (str "443e922e685ab5b904af6ed5f897480e318d1fbc3"
                                    "e158b9092b18fcaf24f7c7b")
                  :target_tag "B"}
                 (dissoc (get match-map "/1.log") :source_time :target_time)))
          (is (= {:name        "A_only.txt"
                  :presence    "A"
                  :rel_path    "/A_only.txt"
                  :source_hash (str "772d9d76cd398a52ad358958795f9efe9e78f2e"
                                    "94405065cfb13cb4f24f4422b")
                  :source_tag  "A"
                  :target_hash nil
                  :target_tag  "B"
                  :target_time nil}
                 (dissoc (get match-map "/A_only.txt") :source_time)))))

      (testing "Big diff map."

        (let [diff-map (select-keys (tag-diff db "A" "B")
                                    ["/sub/1.txt" "/1.log" "/A_only.txt"])
              shortcut (->> diff-map
                            (tag-diff-manifest)
                            (drop 1)
                            (map (juxt first #(get % 2)))
                            (into {}))]
          (is (= {"/1.log"      "false"
                  "/sub/1.txt"  "true"
                  "/A_only.txt" "true"}
                 shortcut)) 
          (is (= {:changed []
                  :deltas []
                  :name "1.log"
                  :presence "A+B"
                  :rel_path "/1.log"
                  :source_hash (str "443e922e685ab5b904af6ed5f897480e318d1fbc3"
                                    "e158b9092b18fcaf24f7c7b")
                  :source_tag "A"
                  :target_hash (str "443e922e685ab5b904af6ed5f897480e318d1fbc3"
                                    "e158b9092b18fcaf24f7c7b")
                  :target_tag "B"
                  :unidiff []}
                 (dissoc (get diff-map "/1.log")
                         :source_time :target_time)))
          (is (= {:changed [{:at 1 :old "A_only.txt" :tag :delete}]
                  :deltas [{:original {:at 0
                                       :lines ["A_only.txt"]
                                       :span 1}
                            :type :delete}]
                  :name "A_only.txt"
                  :presence "A"
                  :rel_path "/A_only.txt"
                  :source_hash (str "772d9d76cd398a52ad358958795f9efe9e78f2e9"
                                    "4405065cfb13cb4f24f4422b")
                  :source_tag "A"
                  :target_hash nil
                  :target_tag "B"}
                 (dissoc (get diff-map "/A_only.txt")
                         :source_time :target_time :unidiff)))))

      (testing "Fetching and diffing entries."

        (let [entry1 (fetch-entry db "A" "/sub/A_only.log")]
          (is (= "/sub/A_only.log" (:rel_path entry1)))
          (is (= "A_only.log" (:name entry1)))
          (is (= "A" (:tag entry1)))
          (is (= 11 (:stored_b entry1)))
          (is (= 11 (:hashed_b entry1)))
          (is (= (str "930f538105a251f6f979752499ffdb93daac243ea80e94cf81473"
                      "b496ebdb24e")
                 (:short_hash entry1)))
          (is (instance? java.util.Date (:fetched_at entry1)))
          (is (instance? java.util.Date (:write_time entry1))))

        (let [no-diff (entry-diff db "A" "B" "1.log")]
          (is (= {:changed [] :deltas [] :unidiff []} no-diff)))

        (let [match       (tag-match db "A" "B")
              match-manif (tag-match-manifest match)
              diff        (tag-diff db "A" "B")
              diff-manif  (tag-diff-manifest diff)
              diff-manif2 (tag-diff-manifest diff (constantly "don't care!"))
              a           (filterv (comp (partial = "A") :presence)
                                   (vals diff))
              b           (filterv (comp (partial = "B") :presence)
                                   (vals diff))
              both        (filterv (comp (partial = "A+B") :presence)
                                   (vals diff))]
          (is (= 2 (count a)))
          (is (= 2 (count b)))
          (is (= 4 (count both)))
          (is (= 8 (count diff)))
          (is (= (count diff) (dec (count match-manif))))
          (is (= (count diff-manif) (count diff-manif2)))
          (doseq [[path tag mm? s-ts t-ts s-h t-h] (drop 1 match-manif)]
            (is (every? string? [path tag s-ts t-ts mm? s-h t-h]))
            (if (= s-h t-h)
              (is (= "false" mm?))
              (is (= "true" mm?))))
          (doseq [[path tag diff? advice s-ts t-ts] (drop 1 diff-manif2)]
            (is (every? string? [path tag diff? advice s-ts t-ts]))
            (is (= advice "don't care!"))
            (let [{:keys [presence rel_path changed]} (get diff path)]
              (is (= presence tag))
              (is (= path rel_path))
              (is (= (-> changed empty? not str) diff?)))))

        (let [diff1 (entry-diff db "A" "B" "/sub/1.txt")]
          (is (= {:changed [{:at 1
                             :new "B sub tree: 1.txt"
                             :old ""
                             :tag :change}
                            {:at 2
                             :new ""
                             :old "A sub tree: 1.txt"
                             :tag :change}]
                  :deltas [{:original {:at 0
                                       :lines ["" "A sub tree: 1.txt"]
                                       :span 2}
                            :revised {:at 0
                                      :lines ["B sub tree: 1.txt"]
                                      :span 1}
                            :type :change}]}
                 (dissoc diff1 :unidiff)))))

      (finally
        (raze! db)
        (thanks-for-the-file! tmp)))))
