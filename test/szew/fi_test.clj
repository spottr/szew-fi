; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "File tree indexer - app tests."}
 szew.fi-test
 (:require
   [szew.fi
    :refer [storage prep! init! ping! summary! store! matches! manifest!
            diffs! import! export! erase! halt!]]
   [szew.fi.entry :as entry]
   [szew.fi.index-test :refer [file-please! thanks-for-the-file!]]
   [clojure.test :refer [deftest testing is]]
   [szew.io.util :as util]))

(deftest Some-UI-stuff-tests

  (let [tmp (file-please!)
        abs (.getCanonicalPath ^java.io.File tmp)
        t-A (entry/tag-top "A" "datasets/tree_root/A")
        t-B (entry/tag-top "B" "datasets/tree_root/B")]

    (try

      (testing "Starting the thing."
        (is (nil? storage))
        (prep! abs)
        (init!)
        (is (not (nil? storage)))
        (is (= :szew.fi/pong @(ping!)))
        (is (= [] @(summary!))))

      (testing "Putting things in."
        (is (= 6 @(store! (entry/gather t-A))))
        (is (= 6 @(store! (entry/gather t-B))))
        (is (= [{:tag "A" :count 6}
                {:tag "B" :count 6}]
               @(summary!)))
        )

      (testing "Getting things out."
        (let [ms ((util/maps-maker) @(matches! "A" "B"))
              a  (filterv (comp (partial = "A") :presence) ms)
              b  (filterv (comp (partial = "B") :presence) ms)
              ab (filterv (comp (partial = "A+B") :presence) ms)]
          (is (= 2 (count a)))
          (is (= 2 (count b)))
          (is (= 4 (count ab))))

        (let [ms ((util/maps-maker) @(manifest! "A" "B"))
              a  (filterv (comp (partial = "A") :presence) ms)
              b  (filterv (comp (partial = "B") :presence) ms)
              ab (filterv (comp (partial = "A+B") :presence) ms)]
          (is (= 2 (count a)))
          (is (= 2 (count b)))
          (is (= 4 (count ab))))

        (let [ds @(diffs! "A" "B")]
          (is (= 1 (count ds)))))

      (testing "Dump and pump."
        @(export! abs)
        @(erase!)
        (is (= org.h2.jdbc.JdbcSQLSyntaxErrorException (class @(summary!))))
        @(import! abs)
        (is (= [{:tag "A" :count 6}
                {:tag "B" :count 6}]
               @(summary!))))

      (finally

        (testing "Clearing up."
          (is (not (nil? @(erase!))))
          (halt!)
          (is (nil? storage))
          )

        (thanks-for-the-file! tmp)))))
