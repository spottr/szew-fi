-- :name drop-fi!
-- :command :execute
-- :result :raw
-- :doc Drops file index table... might need to drop-indexes! first!
DROP TABLE IF EXISTS FILE_INDEX;

-- :name create-fi!
-- :command :execute
-- :result :raw
-- :doc Creates file index in table of given name.
CREATE TABLE IF NOT EXISTS FILE_INDEX (
  TAG        VARCHAR(80) NOT NULL,
  SHORT_HASH VARCHAR(64),
  SIZE       BIGINT NOT NULL,
  HASHED_B   BIGINT NOT NULL,
  STORED_B   BIGINT NOT NULL,
  WRITE_TIME TIMESTAMP NOT NULL,
  PATH       VARCHAR(4096) NOT NULL,
  NAME       VARCHAR(4096) NOT NULL,
  PARENT     VARCHAR(4096) NOT NULL,
  FETCHED_AT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONTENT    CLOB,
  ENCODING   VARCHAR(32) NOT NULL,
  TOP_PATH   VARCHAR(4096) NOT NULL,
  REL_PATH   VARCHAR(4096) NOT NULL,
  REL_PARENT VARCHAR(4096) NOT NULL);

-- :name drop-indexes!
-- :command :execute
-- :result :raw
-- :doc Drop all indices on file index.
DROP INDEX IF EXISTS FILE_INDEX_TAG_IDX;
DROP INDEX IF EXISTS FILE_INDEX_TAG_PATH_IDX;
DROP INDEX IF EXISTS FILE_INDEX_TAG_REL_PATH_IDX;
DROP INDEX IF EXISTS FILE_INDEX_REL_PATH_IDX;

-- :name create-indexes!
-- :command :execute
-- :result :raw
-- :doc Create all indices on file index.
CREATE INDEX IF NOT EXISTS FILE_INDEX_TAG_IDX ON
FILE_INDEX(TAG);

CREATE INDEX IF NOT EXISTS FILE_INDEX_TAG_PATH_IDX ON
FILE_INDEX(TAG, PATH);

CREATE INDEX IF NOT EXISTS FILE_INDEX_TAG_REL_PATH_IDX ON
FILE_INDEX(TAG, REL_PATH);

CREATE INDEX IF NOT EXISTS FILE_INDEX_REL_PATH_IDX ON
FILE_INDEX(REL_PATH);

-- :name insert-entry<!
-- :command :execute
-- :result :affected
-- :doc Inserts single entry (map) into the FILE_INDEX table.
INSERT INTO FILE_INDEX (TAG,
                        SHORT_HASH,
                        SIZE,
                        HASHED_B,
                        STORED_B,
                        WRITE_TIME,
                        PATH,
                        NAME,
                        PARENT,
                        CONTENT,
                        ENCODING,
                        TOP_PATH,
                        REL_PATH,
                        REL_PARENT)
                VALUES (:tag,
                        :short_hash,
                        :size,
                        :hashed_b,
                        :stored_b,
                        :write_time,
                        :path,
                        :name,
                        :parent,
                        :content,
                        :encoding,
                        :top_path,
                        :rel_path,
                        :rel_parent);

-- :name insert-entries<!
-- :command :execute
-- :result :affected
-- :doc Inserts multiple entries (vectors of vals) into the FILE_INDEX table.
INSERT INTO FILE_INDEX (TAG,
                        SHORT_HASH,
                        SIZE,
                        HASHED_B,
                        STORED_B,
                        WRITE_TIME,
                        PATH,
                        NAME,
                        PARENT,
                        CONTENT,
                        ENCODING,
                        TOP_PATH,
                        REL_PATH,
                        REL_PARENT)
                VALUES :tuple*:entries;

-- :name list-index
-- :command :query
-- :result :many
-- :doc Lists all files in the index with their hashes
SELECT TAG, NAME, PATH, SHORT_HASH, PARENT, FETCHED_AT, WRITE_TIME,
       REL_PATH, REL_PARENT, HASHED_B, STORED_B
FROM FILE_INDEX
ORDER BY TAG, PATH;

-- :name list-index-by-tag
-- :command :query
-- :result :many
-- :doc List all records indexed under tag.
SELECT TAG, NAME, PATH, SHORT_HASH, PARENT, FETCHED_AT, WRITE_TIME,
       REL_PATH, REL_PARENT, HASHED_B, STORED_B, ENCODING, SIZE, TOP_PATH
FROM FILE_INDEX
WHERE TAG=:tag
ORDER BY TAG, PATH;

-- :name entry-by-tag-path
-- :command :query
-- :result :one
-- :doc Return full entry by tag and path or rel_path combination
SELECT *
FROM FILE_INDEX
WHERE (TAG=:tag AND REL_PATH=:path) OR (TAG=:tag AND PATH=:path);

-- :name tag-v-tag
-- :command :query
-- :result :many
-- :doc Diff two tags on their rel_path fields. See source column for indication.
SELECT A.NAME, A.REL_PATH, TO_CHAR(A.TAG||'+'||B.TAG) PRESENCE,
       A.WRITE_TIME SOURCE_TIME, B.WRITE_TIME TARGET_TIME,
       TO_CHAR(:tag1) SOURCE_TAG, TO_CHAR(:tag2) TARGET_TAG,
       A.SHORT_HASH SOURCE_HASH, B.SHORT_HASH TARGET_HASH
FROM (SELECT NAME, REL_PATH, TAG, WRITE_TIME, SHORT_HASH
      FROM FILE_INDEX
      WHERE TAG=:tag1) A
INNER JOIN (SELECT NAME, REL_PATH, TAG, WRITE_TIME, SHORT_HASH
            FROM FILE_INDEX
            WHERE TAG=:tag2) B
ON (A.REL_PATH=B.REL_PATH)

UNION ALL

SELECT A.NAME, A.REL_PATH, A.TAG PRESENCE,
       A.WRITE_TIME SOURCE_TIME, NULL TARGET_TIME,
       TO_CHAR(:tag1) SOURCE_TAG, TO_CHAR(:tag2) TARGET_TAG,
       A.SHORT_HASH SOURCE_HASH, NULL TARGET_HASH
FROM (SELECT NAME, REL_PATH, TAG, WRITE_TIME, SHORT_HASH
      FROM FILE_INDEX
      WHERE TAG=:tag1) A
LEFT JOIN (SELECT NAME, REL_PATH, WRITE_TIME, SHORT_HASH
           FROM FILE_INDEX
           WHERE TAG=:tag2) B
ON (A.REL_PATH = B.REL_PATH)
WHERE B.REL_PATH IS NULL

UNION ALL

SELECT B.NAME, B.REL_PATH, B.TAG PRESENCE,
       NULL SOURCE_TIME, B.WRITE_TIME TARGET_TIME,
       TO_CHAR(:tag1) SOURCE_TAG, TO_CHAR(:tag2) TARGET_TAG,
       NULL SOURCE_HASH, B.SHORT_HASH TARGET_HASH
FROM (SELECT NAME, REL_PATH, WRITE_TIME, SHORT_HASH
      FROM FILE_INDEX
      WHERE TAG=:tag1) A
RIGHT JOIN (SELECT NAME, REL_PATH, TAG, WRITE_TIME, SHORT_HASH
            FROM FILE_INDEX
            WHERE TAG=:tag2) B
ON (A.REL_PATH = B.REL_PATH)
WHERE A.REL_PATH IS NULL;

-- :name delete-by-tag!
-- :command :execute
-- :result :affected
-- :doc Deletes all entries with given tag.
DELETE FROM FILE_INDEX
WHERE TAG=:tag;

-- :name median-size-by-tag
-- :command :query
-- :result :many
-- :doc Give median size in tag (in bytes).
SELECT SIZE MEDIAN_SIZE
FROM FILE_INDEX
WHERE TAG=:tag
ORDER BY SIZE
LIMIT 1 OFFSET (SELECT ROUND(COUNT(*)/2)
                FROM FILE_INDEX
                WHERE TAG=:tag);

-- :name dups-by-tag
-- :command :query
-- :result :many
-- :doc List files in tag with hash happening more than once, ordered by hash.
SELECT TAG, NAME, PATH, SHORT_HASH, PARENT, FETCHED_AT, WRITE_TIME,
       PARENT, REL_PATH, REL_PARENT, HASHED_B, STORED_B
FROM FILE_INDEX
WHERE TAG=:tag AND SHORT_HASH IN (
  SELECT SHORT_HASH
  FROM FILE_INDEX
  WHERE TAG=:tag AND SHORT_HASH IS NOT NULL
  GROUP BY SHORT_HASH, HASHED_B
  HAVING COUNT(SHORT_HASH)>1 AND COUNT(HASHED_B)>1
)
ORDER BY SHORT_HASH;

-- :name tag-counts
-- :command :query
-- :result :many
-- :doc List existing tags with file counts.
SELECT TAG, COUNT(TAG) AS COUNT
FROM FILE_INDEX
GROUP BY TAG
ORDER BY TAG;

-- :name tag-count
-- :command :query
-- :result :one
-- :doc List count for given tag.
SELECT COUNT(TAG) AS COUNT
FROM FILE_INDEX
WHERE TAG=:tag;

-- :name biggies-by-tag
-- :command :query
-- :result :many
-- :doc List files over 16KiB.
SELECT *
FROM FILE_INDEX
WHERE TAG=:tag AND SIZE>16384
ORDER BY PATH;

-- :name over-size-by-tag
-- :command :query
-- :result :many
-- :doc List files over given size
SELECT *
FROM FILE_INDEX
WHERE TAG=:tag AND SIZE>:size
ORDER BY PATH;

-- :name size-by-tag-path
-- :command :query
-- :result :many
-- :doc List total size in bytes and file count of tag.
SELECT SUM(SIZE) AS SIZE, COUNT(PATH) AS COUNT
FROM FILE_INDEX
WHERE TAG=:tag AND PATH LIKE :path;
