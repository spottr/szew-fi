(defproject szew/fi "0.3.6-SNAPSHOT"

  :description "Index and compare file trees."
  :url "https://bitbucket.org/spottr/szew-fi"

  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}

  :dependencies [[org.clojure/clojure "1.12.0"]
                 [org.clojure/core.async "1.7.701"]
                 ;; Szew stuff
                 [szew/io "0.5.7" :exclusions [orc.clojure/clojure]]
                 [szew/h2 "0.4.0"]
                 [szew/repl "0.3.10"]
                 ;; Integrant
                 [integrant "0.13.1"]
                 ;; diff report writer
                 [hiccup "1.0.5"]
                 ;; diffing is a solved problem
                 [com.googlecode.java-diff-utils/diffutils "1.3.0"]
                 ;; Logging infrastructure!
                 [ch.qos.logback/logback-classic "1.5.16"]
                 [ch.qos.logback/logback-core "1.5.16"]
                 [org.slf4j/slf4j-api "2.0.16"]
                 [org.slf4j/log4j-over-slf4j "2.0.16"]
                 [org.clojure/tools.logging "1.3.0"]
                 ;; Environment variables in Clojure
                 [environ "1.2.0"]
                 ;; SQL stuffs
                 [org.clojure/java.jdbc "0.7.12"]
                 [com.h2database/h2 "2.3.232"]
                 [com.layerware/hugsql "0.5.3"]]

  :profiles {:dev {:dependencies [[criterium "0.4.6"]
                                  [nrepl "1.3.1"]]
                   :plugins [[lein-codox "0.10.6"]]
                   :source-paths ["dev"]}
             :1.9 {:dependencies [[org.clojure/clojure "1.9.0"]]}
             :uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}}

  ;:global-vars {*warn-on-reflection* false
  ;              *assert* true}

  :aot [clojure.tools.logging.impl ;; aot or die!!!11
        szew.fi.diff
        szew.fi.entry
        szew.fi.index
        szew.fi
        szew.repl]

  :main szew.repl

  :repl-options {:init-ns user
                 :timeout 120000}

  :codox {:project {:name "szew/fi"}
          :namespaces [#"^szew\.fi.*$"]
          ;; that was fun:
          :source-uri
          ~(str "https://bitbucket.org/spottr/szew-fi/src/"
                "873dec3ea81b27b600a3debaf422eb6d22487d24" ;; 0.3.5
                "/{filepath}#{basename}-{line}")}

  :jar-exclusions [#"logback-test\.xml"]

  :jvm-opts [])

