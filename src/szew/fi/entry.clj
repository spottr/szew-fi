; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Files into index entries."}
 szew.fi.entry
 (:gen-class)
 (:require
   [szew.io :as io :refer [in! hasher]]
   [clojure.java.io :as clj.io]
   [clojure.string :as string]
   [clojure.tools.logging :as log]
   [clojure.spec.alpha :as s])
 (:import
   [clojure.lang IFn]
   [java.io File FileNotFoundException]
   [java.nio.file Files]
   [java.util Date]))

;; ## Reading the filesystem

(s/def ::top-path
  (s/or :string (s/and string? (complement empty?))
        :file   (partial instance? File)))

(s/def ::tag (s/and string? (complement empty?)))

(s/def ::hash-bytes
  (s/or :size    (s/and integer? (complement neg?))
        :keyword #{:full :skip}))

(s/def ::store-up-to ::hash-bytes)

(s/def ::encoding (s/and string? (complement empty?)))

(s/def ::entry
  (s/keys :req-un [::top-path ::tag ::hash-bytes ::store-up-to ::encoding]))

(s/def ::short_hash (s/nilable string?))

(s/def ::size ::hash-bytes)

(s/def ::hashed_b (s/and integer? (complement neg?)))

(s/def ::stored_b ::hashed_b)

(s/def ::write_time (partial instance? Date))

(s/def ::path (s/and string? (complement empty?)))

(s/def ::name (s/and string? (complement empty?)))

(s/def ::content (s/nilable string?))

(s/def ::top_path (s/and string? (complement empty?)))

(s/def ::rel_path (s/and string? (complement empty?)))

(s/def ::rel_parent string?)

(s/def ::entry-produce
  (s/keys :req-un [::tag ::short_hash ::size ::hashed_b ::stored_b
                   ::write_time ::path ::name ::parent ::content
                   ::encoding ::top_path ::rel_path ::rel_parent]))

(defrecord Entry [top-path tag hash-bytes store-up-to encoding]

  io/Input

  (in! [spec source]
    (when-not (s/valid? ::entry spec)
      (throw
        (ex-info "Entry spec validation failed!"
                 {:explanation (s/explain-data ::entry spec)
                  :source      source})))
    (io!
      "Happily mutable: multiple file reads."
      (let [a-file    ^java.io.File (-> source
                                        (clj.io/as-file)
                                        (.getCanonicalPath)
                                        (clj.io/as-file))
            t-path    (.getCanonicalPath (clj.io/as-file top-path))
            parent    (-> a-file (.getParentFile) (.getCanonicalPath))
            size      (.length a-file)
            h-sample  (cond
                        (or (= :skip hash-bytes) (= 0 hash-bytes))
                        0
                        (or (= :full hash-bytes) (>= hash-bytes size))
                        size
                        :else
                        hash-bytes)
            s-sample  (cond
                        (or (= :skip store-up-to)
                            (and (number? store-up-to)
                                 (or (= 0 store-up-to)
                                     (< store-up-to size))))
                        0
                        (or (= :full store-up-to) (>= store-up-to size))
                        size
                        :else
                        store-up-to)
            path      (.getCanonicalPath a-file)
            readable? (Files/isReadable (.toPath a-file))]
        (try
          (assert (.contains path t-path) "top-path must be in path")
          {:tag        tag
           :short_hash (when (and (pos? h-sample) readable?)
                         (in! (hasher {:hash-name   "SHA-256"
                                       :sample-size hash-bytes})
                                 a-file))
           :size       size
           :hashed_b   h-sample
           :stored_b   s-sample
           :write_time (Date. (.lastModified a-file))
           :path       path
           :name       (.getName a-file)
           :parent     parent
           :content    (when (and (pos? s-sample) readable?)
                         (slurp a-file :encoding encoding))
           :encoding   encoding
           :top_path   t-path
           :rel_path   (string/replace path t-path "")
           :rel_parent (string/replace parent t-path "")}
          (catch FileNotFoundException _
            (log/warn "FileNotFound:" t-path)
            nil)
          (catch AssertionError ae
            (throw (ex-info "Argument error." {:spec spec :path path} ae)))
          (catch Exception ex
            (throw (ex-info "Entry io error."
                            {:spec spec :path path} ex)))))))

  IFn

  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (in! spec (first args)))

  (invoke [spec target]
    (in! spec target)))

(defn entry
  "Entry be like:

    {:top-path    String | File
     :tag         String
     :hash-bytes  positive integer | :full | :skip
     :store-up-to positive integer | :full | :skip
     :encoding    String}

  The top-path will be removed from CanonicalPath of file and field calculated
  this way will be used for tag vs tag comparison in the db (relative path).

  Initially this is only planned for small (<16KiB) files so we are also trying
  to capture contents of said files (as text!) and SHA-256 to that size limit.
  You can parametrize both settings separately, but please be vary of using
  :full on uncertain inputs. It really works best for wee files.
  "
  ([]
   (let [t-p (.getCanonicalPath (clj.io/as-file "."))]
     (map->Entry {:hash-bytes  16384
                  :store-up-to 16384
                  :encoding    "UTF-8"
                  :top-path    t-p
                  :tag         t-p})))
  ([spec]
   {:pre  [(s/nilable map?)]
    :post [(s/valid? ::entry %)]}
   (if (:top-path spec)
     (let [t-p (.getCanonicalPath (clj.io/as-file (:top-path spec)))]
       (into (entry) (assoc spec :top-path t-p)))
     (into (entry) spec))))

(defn tag-top
  "Create entry with given tag and top-path. Use opts map for other fields.
  "
  ([tag top-path]
   {:pre  [(s/valid? ::tag tag) (s/valid? ::top_path top-path)]
    :post [(s/valid? ::entry %)]}
   (entry {:tag tag :top-path top-path})) 
  ([tag top-path opts]
   {:pre  [(s/valid? ::tag tag)
           (s/valid? ::top_path top-path)
           (s/valid? map? opts)]
    :post [(s/valid? ::entry %)]}
   (entry (merge opts {:tag tag :top-path top-path}))))

;; ## Do we want it? Setting bar really low, Java7 low.

(let [empty-opts (into-array java.nio.file.LinkOption [])]
  (defn of-interest?
    "a-file -> bool

    Simplest criteria: is it a file, can we read it.
    "
    [^File file]
    {:pre  [(instance? File file)]
     :post [(boolean? %)]}
    (let [path (.toPath file)]
      (and (Files/isRegularFile path empty-opts)
           (Files/isReadable path)))))

(defn gather
  "Top entry, pruning and reading preds in. Seq of entries out.
  "
  ([entry-point follow? process?]
   {:pre  [(s/valid? ::entry entry-point) (ifn? follow?) (ifn? process?)]
    :post [(or (s/valid? (s/coll-of ::entry-produce) %)
               (s/explain (s/coll-of ::entry-produce) %))]}
   (let [pxf (partial sequence
                      (comp (filter process?)
                            (map (partial io/in! entry-point))
                            (filter (complement nil?))))]
     (io/in! (io/files {:follow? follow? :processor pxf})
             (:top-path entry-point))))
  ([entry-point process?]
   (gather entry-point (constantly true) process?))
  ([entry-point]
   (gather entry-point (constantly true) of-interest?)))
