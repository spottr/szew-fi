; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Diffing utilities of last resort. Thin wrapper over difflib."}
 szew.fi.diff
 (:gen-class)
 (:require
   [clojure.spec.alpha :as s])
 (:import
   [difflib DiffUtils
    Delta
    Delta$TYPE
    Chunk
    Patch
    DiffRowGenerator
    DiffRowGenerator$Builder
    DiffRow
    DiffRow$Tag]))

;; ## Patching & unidiff!

(s/def ::lines
  (s/nilable (s/coll-of string? :into [])))

(defn ->Patch
  "Diff pair of line sequences/vectors. Gives Patch back.
  "
  [original revised]
  {:pre  [(s/valid? ::lines original), (s/valid? ::lines revised)]
   :post [(instance? Patch %)]}
  (DiffUtils/diff (vec original) (vec revised)))

(s/def ::path (s/and string? (complement empty?)))

(s/def ::line-count (s/and number? (complement neg?)))

(defn unidiff
  "Return Unified Diff (unidiff) format as vector of strings.

  Arguments:

  * orig-path: original file path and optional timestamp, for diff header
  * new-path: revised file path and optional timestamp, for diff header
  * orig-lines: original lines used as base for diff
  * a-Patch: Patch instance of original and revised lines
  * context-lines: number of lines to include as context.
  "
  [orig-path rev-path orig-lines a-Patch context-lines]
  {:pre  [(s/valid? ::path orig-path)
          (s/valid? ::path rev-path)
          (s/valid? ::lines orig-lines)
          (instance? Patch a-Patch)
          (s/valid? ::line-count context-lines)]
   :post [(s/valid? ::lines %)]}
  (vec (DiffUtils/generateUnifiedDiff orig-path
                                      rev-path
                                      (vec orig-lines)
                                      a-Patch
                                      context-lines)))

;; ## Diffing: line by line, side by side.

(s/def ::tag
  (s/or :insert #{:insert}
        :delete #{:delete}
        :change #{:change}
        :equal  #{:equal}))

(s/def ::at (s/and number? (complement neg?)))

(s/def ::line-string string?)

(s/def ::new ::line-string)

(s/def ::old ::line-string)

(s/def ::line-map
  (s/or :insert
        (s/and map?
               (comp (partial = :insert) :tag)
               (s/keys :req-un [::tag ::new ::at]))
        :delete
        (s/and map?
               (comp (partial = :delete) :tag)
               (s/keys :req-un [::tag ::old ::at]))
        :change
        (s/and map?
               (comp (partial = :change) :tag)
               (s/keys :req-un [::tag ::new ::old ::at]))
        :equal
        (s/and map?
               (comp (partial = :equal) :tag)
               (s/keys :req-un [::tag ::old ::at]))))

(s/def ::line-maps
  (s/nilable (s/coll-of ::line-map :into [])))

(defn line-maps
  "Run diff line by line and return vector of line-by-line changes as maps.

  Each map will look like this:

    {:tag :insert :new String :at positive long}

    {:tag :delete :old String :at positive long}

    {:tag :change :old String :new String :at positive long}

    {:tag :equal :old String :at positive long}

  What we call quick & dirty here, may be fun for quick side by side.
  "
  [original revised]
  {:pre  [(or (s/valid? ::lines original) (s/explain ::lines original))
          (or (s/valid? ::lines revised) (s/explain ::lines revised))]
   :post [(or (s/valid? ::line-maps %) (s/explain ::line-maps %))]}
  (if (and (empty? original) (empty? revised))
    []
    (let [builder-gen  (doto (DiffRowGenerator$Builder.)
                         (.showInlineDiffs false)
                         (.columnWidth 16192))
          builder      (.build builder-gen)
          line-by-line (fn row-proc [^DiffRow row line-no]
                         (let [tag ^DiffRow$Tag (.getTag row)]
                           (condp = tag
                             DiffRow$Tag/INSERT
                             {:tag :insert
                              :new (.getNewLine row)
                              :at  line-no}
                             DiffRow$Tag/DELETE
                             {:tag :delete
                              :old (.getOldLine row)
                              :at  line-no}
                             DiffRow$Tag/CHANGE
                             {:tag :change
                              :old (.getOldLine row)
                              :new (.getNewLine row)
                              :at  line-no}
                             DiffRow$Tag/EQUAL
                             {:tag :equal
                              :old (.getOldLine row)
                              :at  line-no}
                             ; default: shout!
                             (throw (ex-info "Danger Zone: Wrong DiffRow!"
                                             {:tag :mu
                                              :row  row
                                              :at   line-no})))))]
      (mapv line-by-line (.generateDiffRows builder
                                            (vec original)
                                            (vec revised))
            (map (comp inc long) (range))))))

;; ## Diffing: Serious business, deltas and chunks, yo!

(defn ->Deltas
  "Run Diff on pair of line sequences/vectors. Gives vector of Deltas.
  "
  [original revised]
  {:pre  [(s/valid? ::lines original)
          (s/valid? ::lines revised)]
   :post [(s/valid? (s/coll-of (partial instance? Delta)) %)]}
  (vec (.getDeltas ^Patch (->Patch original revised))))

(s/def ::span (s/and number? pos?))

(s/def ::chunk-map
  (s/and map?
         (s/keys :req-un [::at ::span ::lines])))

(defn Chunk->map
  "Creates a hash-map from Chunk instance.

  Returned map will be something like this:

    {:at    long
     :span  long
     :lines vector of Strings}
  "
  [^Chunk a-Chunk]
  {:pre  [(instance? Chunk a-Chunk)]
   :post [(s/valid? (s/nilable ::chunk-map) %)]}
  (when (seq (.getLines a-Chunk))
    {:at    (long (.getPosition a-Chunk))
     :span  (long (.size a-Chunk))
     :lines (vec (.getLines a-Chunk))}))

(s/def ::type #{:insert :delete :change})

(s/def ::revised ::chunk-map)

(s/def ::original ::chunk-map)

(s/def ::delta-map
  (s/or :insert
        (s/and map?
               (comp (partial = :insert) :type)
               (s/keys :req-un [::type ::revised]))
        :delete
        (s/and map?
               (comp (partial = :delete) :type)
               (s/keys :req-un [::type ::original]))
        :change
        (s/and map?
               (comp (partial = :change) :type)
               (s/keys :req-un [::type ::revised ::original]))))

(defn Delta->map
  "Creates a hash-map from Delta instance.

  Returned map will look something like this:

  {:type :insert :revised chunk-map}

  {:type :delete :original chunk-map}

  {:type :change :original chunk-map :revised chunk-map}
  "
  [^Delta a-Delta]
  {:pre  [(instance? Delta a-Delta)]
   :post [(s/valid? ::delta-map %)]}
  (condp = (.getType a-Delta)
    Delta$TYPE/INSERT
    {:type     :insert
     :revised  (Chunk->map (.getRevised a-Delta))}
    Delta$TYPE/DELETE
    {:type     :delete
     :original (Chunk->map (.getOriginal a-Delta))}
    Delta$TYPE/CHANGE
    {:type     :change
     :original (Chunk->map (.getOriginal a-Delta))
     :revised  (Chunk->map (.getRevised a-Delta))}
    ; default: shout!
    (throw (ex-info "Something unexpected." {:delta a-Delta}))))

(defn delta-maps
  "Run diff, get Deltas, publish them as hash-maps in a vector.

  LPT: consult docs of Delta->map and Chunk->map.
  "
  [original revised]
  {:pre  [(s/valid? ::lines original)
          (s/valid? ::lines revised)]
   :post [(s/valid? (s/coll-of ::delta-map) %)]}
  (if (or (and (empty? original) (empty? revised)) (= original revised))
    []
    (mapv Delta->map (->Deltas original revised))))
