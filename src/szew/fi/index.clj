; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Cold storage."}
 szew.fi.index
 (:gen-class)
 (:require
   [szew.h2.java-jdbc :refer [raze! dump! pump!]]
   [szew.h2.util :refer [de-clob]]
   [szew.fi.diff :as diff]
   [szew.io.util :as util]
   [hugsql.core :as hugsql]
   [clojure.tools.logging :as log]
   [clojure.java.jdbc :as jdbc]
   [hiccup.page :refer [html5]]
   [clojure.string :as string]))

(declare create-fi! create-indexes!
         insert-entries<!
         tag-counts tag-count tag-v-tag
         entry-by-tag-path
         delete-by-tag!)

;; Index creation, indexing, selectors defined in raw SQL.
;; hugsql will look for this file in resources/
(hugsql/def-db-fns "fi.sql")

;; ## Propagating the database

(defn seed!
  "Try to set up application database on given connection.
  "
  [db]
  (try
    (log/debug "Probing existing database.")
    (tag-counts db {})
    (log/debug "Already seeded, nothing to do!")
    (catch Exception _
      (log/debug "Creating fi table.")
      (create-fi! db {})
      (log/debug "Creating fi indexes.")
      (create-indexes! db {})
      (log/debug "Application seeded."))))

(defn delete-tag!
  "Delete all entries for given tag, return number of records deleted.
  "
  [db tag]
  (try
    (jdbc/with-db-transaction [tx db]
      (let [hits (:count (tag-count tx {:tag tag}))]
        (when (pos? hits)
          (log/debug "Deleting:" {tag hits})
          (delete-by-tag! tx {:tag tag}))))
    (catch Exception ex
      (throw (ex-info "Error when sanitizing!"
                      {:db  db
                       :tag tag}
                      ex)))))

(let [row (partial util/map->vec [:tag :short_hash :size
                                  :hashed_b :stored_b :write_time
                                  :path :name :parent :content :encoding
                                  :top_path :rel_path :rel_parent])]
  (defn store-batched!
    "Insert all entries from batched sequence. Return number of inserts.
    "
    [db batched]
    (io!
      "Will mutate database here!"
      (log/debug "Storing:" (frequencies (map :tag batched)))
      (jdbc/with-db-transaction [tx db]
        (insert-entries<! tx {:entries (mapv row batched)})))))

(defn erase-database!
  "Remove all data from the databse.
  "
  [db]
  (io!
    "This kills the database."
    (log/debug "Erasing database.")
    (raze! db)))

(defn export-archive!
  "Archive database into a GZip export file.
  "
  [db path]
  (io!
    "This dumps the database."
    (log/debug "Exporting to:" path)
    (dump! db path true)))

(defn import-archive!
  "Restore database from a GZip export file.
  "
  [db path]
  (io!
    "This pumps the database."
    (log/debug "Importing from:" path)
    (pump! db path true)))

;; ## Reading back from database - entries and diffs

(defn fetch-entry
  "Get db connection, tag and path/rel_path, return entry with content
  split into vector of lines (if content captured, otherwise vector of single,
  empty string).
  "
  [db tag path]
  (jdbc/with-db-transaction [tx db]
    (let [query {:tag tag :path path}]
    (when-let [entry (entry-by-tag-path tx query)]
      (if (nil? (:content entry))
        (assoc entry :content [""])
        (-> entry
            (de-clob [:content])
            (update-in [:content] (fn [c] (string/split (str c) #"\r?\n")))))))))

(defn entry-diff
  "Compare contents of a file between tags: changed lines, delta and unidiff.

    {:changed x :deltas x :unidiff x}

  * db connection, source-tag and target-tag are used to lookup entry
  of rel-path. Tag + path combo used for lookup.
  "
  [db source-tag target-tag rel-path]
  (let [source  (fetch-entry db source-tag rel-path)
        target  (fetch-entry db target-tag rel-path)
        lines   (filterv (comp (partial not= :equal) :tag)
                         (diff/line-maps (:content source) (:content target)))
        deltas  (diff/delta-maps (:content source) (:content target))
        a-patch (diff/->Patch (:content source) (:content target))
        fmt     (comp (partial apply (partial format "%s\t''%s''\t%s@%s"))
                      (juxt :rel_path :write_time :tag :fetched_at))
        a-diff  (vec (diff/unidiff (fmt source)
                                   (fmt target)
                                   (:content source)
                                   a-patch
                                   1))]
    {:changed lines
     :deltas  deltas
     :unidiff a-diff}))

(defn tag-match
  "Returns a hash-map of rel_path := tag-v-tag summary.

  * db is a database connection
  * source-tag and target-tag to generate the differences.
  "
  [db source-tag target-tag]
  (->> (tag-v-tag db {:tag1 source-tag
                      :tag2 target-tag})
       (mapv (juxt :rel_path identity))
       (into (hash-map))))

(defn tag-match-manifest
  "Generate a manifest of all files from source and target tags, with last
  modification date for entries.
  "
  [tag-matches]
  (->> (vals tag-matches)
       (mapv (juxt :rel_path
                   :presence
                   (comp str #(not= (:source_hash %) (:target_hash %)))
                   (comp str :source_time)
                   (comp str :target_time)
                   (comp str :source_hash)
                   (comp str :target_hash)))
       (sort-by first)
       (cons ["Relative Path" "Presence" "Mismatch?"
              "Source Timestamp" "Target Timestamp" "Source Hash" "Target Hash"])))

(defn tag-diff
  "Returns a hash-map of rel_path := difference summary.

  * db is a database connection
  * source-tag and target-tag to generate the differences.
  "
  [db source-tag target-tag]
  (let [differ (partial entry-diff db source-tag target-tag)]
    (->> (tag-v-tag db {:tag1 source-tag
                        :tag2 target-tag})
         (mapv (comp (partial apply merge)
                     (juxt identity (comp differ :rel_path))))
         (mapv (juxt :rel_path identity))
         (into (hash-map)))))

(defn super-good-advice
  "Default advisor! Get entry-diff, give Super Good Advice(TM).
  "
  [entry-diff]
  (cond (and (= (str (:source_tag entry-diff) "+" (:target_tag entry-diff))
                (:presence entry-diff))
             (empty? (:changed entry-diff)))
        "IGNORE (Reason: No diff)"
        (= (:source_tag entry-diff) (:presence entry-diff))
        "DELETE (Reason: Not in target)"
        (= (:target_tag entry-diff) (:presence entry-diff))
        "REVIEW (Reason: Only in target)"
        :else
        "REVIEW (Reason: Diff in content)"))

(defn tag-diff-manifest
  "Generate a manifest of all files from source and target tags, with last
  modification date for entries.

  * tag-diffs is a result of tag-diff
  * advisor is a function of tag-diffs entry, that will suggest action.
  "
  ([tag-diffs advisor]
   (->> (vals tag-diffs)
        (mapv (juxt :rel_path
                    :presence
                    (comp str not empty? :unidiff)
                    advisor
                    (comp str :source_time)
                    (comp str :target_time)))
        (sort-by first)
        (cons ["Relative Path" "Presence" "Diff?" "Advisory"
               "Source Timestamp" "Target Timestamp"])))
  ([tag-diffs]
   (tag-diff-manifest tag-diffs super-good-advice)))

;; ## Summaries and reports

(defn summary
  "List tag counts as vector of {:tag String :count integer}
  "
  [db]
  (vec (tag-counts db {})))

(defn matches
  "Produce matches manifest.
  "
  ([db source-tag target-tag]
   (io!
     "And lots of it!"
     (let [t-m (tag-match db source-tag target-tag)]
       (tag-match-manifest t-m)))))

(defn manifests
  "Produces diff manifest.
  "
  ([db source-tag target-tag advisor]
   (io!
     "And lots of it!"
     (let [t-m  (tag-diff db source-tag target-tag)]
       (tag-diff-manifest t-m advisor))))
  ([db source-tag target-tag]
   (manifests db source-tag target-tag super-good-advice)))

(defn diffs
  "Produce diffs as map of relative path to tag-diff entries.
  "
  [db source-tag target-tag]
  (->> (tag-diff db source-tag target-tag)
       (vals)
       (filter (comp (partial = (str source-tag "+" target-tag)) :presence))
       (filter #(not= (:source_hash %) (:target_hash %)))
       (sort-by :rel_path)
       (vec)))

(defn html-diffs
  "Produce string containing HTML summary of diffs present in both tags.
  "
  ([db source target spec]
   (letfn [(style []
             (format
               (str "*{margin:0; padding:0; background:%s;"
                    "font-scale:80%%; color:%s;\n"
                    "font-family:Inconsolata,'Droid Sans Mono',"
                    "monospace;}\n"
                    "h1{font-size:2em;text-align:center;margin:1em;}\n"
                    ".diff{margin:4em;font-size:1.2em;}\n"
                    "p{line-height:1.4;white-space:pre;}\n"
                    "hr{border:1px dotted silver;background-color:snow;}\n"
                    ".head{color:%s;}\n"
                    ".mark{color:%s;}\n"
                    ".in{color:%s;}\n"
                    ".out{color:%s;}\n")
               (-> spec :style (:bg "snow"))
               (-> spec :style (:color "#000033"))
               (-> spec :style (:head "chocolate"))
               (-> spec :style (:mark "blue"))
               (-> spec :style (:in "limegreen"))
               (-> spec :style (:out "orangered"))))
           (page [source target content]
             (html5
               [:head
                [:title (format "diffs: %s v. %s" source target)]
                [:meta {"charset" "utf-8"}]
                [:style (style)]]
               [:body
                [:h1 "diff: " [:span {"class" "out"} source]
                 " v. "
                 [:span {"class" "in"} target]]
                [:hr]
                content]))
           (lines-to-li [content]
             (when (seq content)
               (let [escs  {\< "&lt;" \> "&gt;" \& "&amp;"}
                     lines (mapv #(string/escape % escs) content)]
                 [:div
                  (into [:div.diff]
                        (for [line lines]
                          (cond
                            (or (.startsWith line "+++")
                                (.startsWith line "---"))
                            [:div.head {"class" "head"} line]
                            (= (first line) \+)
                            [:div.in {"class" "in"} line]
                            (= (first line) \-)
                            [:div {"class" "out"} line]
                            (= (first line) \@)
                            [:div {"class" "mark"} line]
                            :else
                            [:div line])))
                  [:hr]])))
           (both [] (str source "+" target))]
     (io!
       "And lots of it!"
       (log/debug "Producing HTML diff for:" source "->" target)
       (->> (tag-diff db source target)
            (vals)
            (filter (comp (partial = (both)) :presence))
            (sort-by :rel_path)
            (map (comp lines-to-li :unidiff))
            (page source target)))))
  ([db source target]
   (html-diffs db source target {})))
