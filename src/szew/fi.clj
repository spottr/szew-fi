; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "File tree indexer - app."}
 szew.fi
 (:gen-class)
 (:require
   [clojure.java.io :as clj.io]
   [clojure.tools.logging :as log]
   [clojure.core.async :as async]
   [clojure.spec.alpha :as s]
   [szew.h2 :as h2]
   [szew.h2.server :as srv]
   [szew.fi.index :as index]
   [integrant.core :as ig]
   [environ.core :refer [env]]))

;; ## State management

(def ^{:doc     "Application state storage."
       :dynamic true}
  storage nil)

(def ^{:doc "Application state configuration."}
  config
  {::store {:path (:database-path env)}
   ::worker {:store (ig/ref ::store)
             :in-buffer 1024}})

(defn command!
  "Tries to do an async/put! on the command channel, returns promise if worked."
  ([header payload]
   (assert (not (nil? (::store @#'storage))) "Store not started!")
   (let [reply (promise)
         sink (get-in @#'storage [::worker :sink!])]
     (log/debug "Issuing command:" header)
     (when (sink [header reply payload])
       reply)))
  ([header]
   (command! header nil)))

(defmulti handle-msg
  (fn dispatch
    [store [header reply payload :as msg]]
    header))

(defmethod handle-msg ::ping
  [store [header reply _ :as msg]]
  (log/debug "Got ping! Sending pong!")
  (when reply
    (deliver reply ::pong)))

(defmethod handle-msg ::store
  [store [header reply payload :as msg]]
  (try
    (log/debug "Store/Start:" (frequencies (map :tag payload)))
    (let [v (index/store-batched! store payload)]
      (log/debug "Insertion:" v)
      (when reply
        (deliver reply v))
      (log/debug "Store/Done:" (frequencies (map :tag payload))))
    (catch Exception ex
      (log/debug "Error in ::store -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::delete
  [store [header reply payload :as msg]]
  (try
    (log/debug "Delete/Start:" payload)
    (let [v (index/delete-tag! store payload)]
      (log/debug "Deleted:" v)
      (when reply
        (deliver reply v))
      (log/debug "Delete/Done:" payload))
    (catch Exception ex
      (log/debug "Error in ::delete -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::summary
  [store [header reply _ :as msg]]
  (try
    (log/debug "Summary/Start")
    (let [v (index/summary store)]
      (when reply
        (deliver reply v))
      (log/debug "Summary/Done"))
    (catch Exception ex
      (log/debug "Error in ::summary -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::matches
  [store [header reply [original revised :as tags] :as msg]]
  (try
    (log/debug "Matches/Start:" original "->" revised)
    (let [v (index/matches store original revised)]
      (when reply
        (deliver reply v))
      (log/debug "Matches/Done:" original "->" revised))
    (catch Exception ex
      (log/debug "Error in ::matches -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::manifests
  [store [header reply [original revised advisor :as tags] :as msg]]
  (try
    (log/debug "Manifests/Start:" original "->" revised)
    (let [a (or advisor index/super-good-advice)
          v (index/manifests store original revised a)]
      (when reply
        (deliver reply v))
      (log/debug "Manifests/Done:" original "->" revised))
    (catch Exception ex
      (log/debug "Error in ::manifests -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::diffs
  [store [header reply [original revised :as tags] :as msg]]
  (try
    (log/debug "Diffs/Start:" original "->" revised)
    (let [v (index/diffs store original revised)]
      (when reply
        (deliver reply v))
      (log/debug "Diffs/Done:" original "->" revised))
    (catch Exception ex
      (log/debug "Error in ::diffs -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::html-diffs
  [store [header reply [original revised spec :as tags] :as msg]]
  (try
    (log/debug "HTML-Diffs/Start:" original "->" revised)
    (let [v (index/html-diffs store original revised spec)]
      (when reply
        (deliver reply v))
      (log/debug "HTML-Diffs/Done:" original "->" revised))
    (catch Exception ex
      (log/debug "Error in ::html-diffs -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::erase
  [store [header reply _ :as msg]]
  (try
    (log/debug "Erase/Start.")
    (let [v (index/erase-database! store)]
      (when reply
        (deliver reply v))
      (log/debug "Erase/Done."))
    (catch Exception ex
      (log/debug "Error in ::erase -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::export
  [store [header reply path :as msg]]
  (try
    (log/debug "Export/Start:" path)
    (let [v (index/export-archive! store path)]
      (when reply
        (deliver reply v))
      (log/debug "Export/Done:" path))
    (catch Exception ex
      (log/debug "Error in ::export -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg ::import
  [store [header reply path :as msg]]
  (try
    (log/debug "Import/Start:" path)
    (let [v (index/import-archive! store path)]
      (when reply
        (deliver reply v))
      (log/debug "Import/Done:" path))
    (catch Exception ex
      (log/debug "Error in ::import -" (.getMessage ex))
      (when reply
        (deliver reply ex)))))

(defmethod handle-msg :default
  [store [header reply payload :as msg]]
  (when reply
    (deliver reply :not-implemented)))

(defmethod ig/init-key ::store
  [kw opts]
  (assert (not (empty? (:path opts))) "DATABASE_PATH not set.")
  (log/debug "Initializing" kw)
  (let [flags {"COMPRESS"      "FALSE"
               "DEFRAG_ALWAYS" "FALSE"
               "EARLY_FILTER"  "TRUE"
               "MV_STORE"      "TRUE"}
        conn  {:datasource (h2/spec->DS (h2/spec (:path opts) flags))}]
      (index/seed! conn)
      conn))

(defmethod ig/halt-key! ::store
  [kw opts]
  (log/debug "Halting" kw)
  nil)

(defmethod ig/init-key ::worker
  [kw opts]
  (log/debug "Initializing" kw)
  (let [input (async/chan (async/buffer (:in-buffer opts)))
        store (:store opts)]
    {:sink! (fn [v] (async/put! input v))
     :sink!! (fn [v] (async/>!! input v))
     :close! (fn [] (log/debug "Called :close!") (async/close! input))
     :thread
     (async/thread
       (loop [msg (async/<!! input)]
         (if (nil? msg)
           (log/info "Input closed, worker finishing.")
           (do
             (handle-msg store msg)
             (recur (async/<!! input)))))
       :done)}))

(defmethod ig/halt-key! ::worker
  [kw opts]
  (log/debug "Halting" kw)
  ((:close! opts)))

(defn prep!
  "Set database-path from given argument or from environment.
  "
  ([]
   (prep! (:database-path env))
   :prepped)
  ([database-path]
   (log/debug "Prepping storage config:" database-path)
   (alter-var-root #'config assoc-in [::store :path] database-path)
   :prepped))

(defn init!
  "Initialize storage var."
  ([]
   (log/debug "Initializing storage.")
   (alter-var-root #'storage (constantly (ig/init @#'config)))
   :initialized))

(defn halt!
  "Halt storage var and nil it."
  ([]
   (log/debug "Halting storage.")
   (ig/halt! storage)
   (alter-var-root #'storage (constantly nil))
   :halted))

(defn restart!
  "First halt, then init storage var."
  ([]
   (halt!)
   (init!)
   :restarted))

;; UI

(defn ping!
  "Try to send a ::ping message to the index. Expect ::pong reply.
  "
  []
  (command! ::ping))

(defn store!
  "Accept sequence of sequences of entries, returns thread channel.

  If sending worked, promise gets returned, otherwise nil. Promise will
  either end up with a number of inserted records or Exception.
  "
  [batch]
  {:pre [(s/valid? (s/coll-of :szew.fi.entry/entry-produce) batch)]}
  (command! ::store batch))

(defn delete!
  "Accept tag, return number of deleted entries.

  If sending worked, promise gets returned, otherwise nil. Promise will
  either end up with a number of inserted records or Exception.
  "
  [tag]
  {:pre [(s/valid? string? tag)]}
  (command! ::delete tag))

(defn summary!
  "Returns promise that will deliver a vector of tags and counts.
  "
  []
  (command! ::summary))

(defn matches!
  "Returns a promise that will deliver matches, vector of vectors, with head.
  "
  ([original revised]
   {:pre [(s/valid? string? original) (s/valid? string? revised)]}
   (command! ::matches [original revised])))

(defn manifest!
  "Returns a promise that will deliver manifest, vector of vectors, with head.
  "
  ([original revised advisor]
   {:pre [(s/valid? string? original)
          (s/valid? string? revised)
          (ifn? advisor)]}
   (command! ::manifests [original revised advisor]))
  ([original revised]
   (manifest! original revised index/super-good-advice)))

(defn diffs!
  "Returns a promise that will deliver diffs, vector of maps.
  "
  [original revised]
  {:pre [(s/valid? string? original) (s/valid? string? revised)]}
  (command! ::diffs [original revised]))

(defn html-diffs!
  "Returns a promise that will return a string of HTML diffs.
  "
  ([original revised spec]
   {:pre [(s/valid? string? original)
          (s/valid? string? revised)
          (s/valid? (s/nilable map?) spec)]}
    (command! ::html-diffs [original revised spec]))
  ([original revised]
    (html-diffs! original revised {})))

(defn erase!
  "Completely erase the database.
  "
  []
  (command! ::erase))

(defn export!
  "Export database to GZip dump.
  "
  [path]
  (let [abs (.getCanonicalPath (clj.io/as-file path))]
    (command! ::export abs)))

(defn import!
  "Import database from GZip dump.
  "
  [path]
  (let [abs (.getCanonicalPath (clj.io/as-file path))]
    (command! ::import abs)))

(defn console!
  "Starts server console over current storage.

  Remembert to 'disconnect' from the console in the browser!"
  []
  (with-open [conn (-> storage ::store :datasource .getConnection)]
    (srv/start-web-server conn)))
