# Change Log
All notable Changed to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
- ...


## [0.3.5] - 2021-02-23
### Changed
- Minor status update printing touchup.
- Version upgrades for Clojure and szew/io.
## [0.3.4] - 2020-05-28
### Changed
- Major dependency version upgrades.
### Fixed
- More graceful handling of unreadable files (skip slurping).
- Better handling of 0-byte files (allows them).
- Database options for newer H2 version (MVCC removed, MV_STORE enabled).
- Additional hash filter in `szew.fi.index/diffs`.
### Added
- `szew.fi/console!` for H2 web console over current storage.

## [0.3.3] - 2019-06-18
### Changed
- Major dependency version upgrades, single test adjusted.

## [0.3.2] - 2018-09-12
### Fixed
- Fix lines spec to allow empty lines in diffs (those happen!).

## [0.3.1] - 2018-09-11
### Added
- Included `szew.repl` for standalone script execution.

## [0.3.0] - 2018-03-11
### Changed
- Specced diff and entry, updated index, extended tests.
- Refactored the entire application to use state management.
- Separation of duties: entry deals with files, index with database.
- User interface is now contained within fi and it's async.

## [0.2.3] - 2016-11-21
### Fixed
- Fixed issue with in memory :db-path; It's now stored and must be a path.

## [0.2.2] - 2016-11-20
### Fixed
- Bug in Harvester: setting old :keep-fn instead of :follow? in szew.io/files.

## [0.2.1] - 2016-11-02
### Fixed
- Included logback-test.xml in the 0.2.0 JAR. Had bad time. Excluded.

## [0.2.0] - 2016-11-01
### Changed
- Rewrote BlueLeaves:
    * Work data (`db-path`) is stored in a in-memory database.
    * All tags are now harvested inside futures, because threads.
    * GZip dump (`gz-path`) is used to seed in-memory database if present.
### Fixed
- Fixed HTML diff generator and improved that output a little.

## [0.1.1] - 2016-10-19
### Fixed
- HTML generation: & was escaped into &nbsp; instead of &amp; ;-(

## [0.1.0] - 2016-10-15
### Initial
- This is the first extraction from private `szew` toolkit.
- `szew.fi.diff` packages `diffutils` library for diff, patch etc.
- `szew.fi` namespace defines filesystem harvester and comparisons.
- Moved from originally used YeSQL to HugSQL, which seems to be maintained.

[Unreleased]: https://bitbucket.org/spottr/szew-fi/branches/compare/master%0D0.3.5#diff
[0.3.5]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.3.5%0D0.3.4#diff
[0.3.4]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.3.5%0D0.3.4#diff
[0.3.3]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.3.3%0D0.3.2#diff
[0.3.2]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.3.2%0D0.3.1#diff
[0.3.1]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.3.1%0D0.3.0#diff
[0.3.0]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.3.0%0D0.2.3#diff
[0.2.3]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.2.3%0D0.2.2#diff
[0.2.2]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.2.2%0D0.2.1#diff
[0.2.1]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.2.1%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.2.0%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.1.1%0D0.1.0#diff
[0.1.0]: https://bitbucket.org/spottr/szew-fi/branches/compare/0.1.0%0De4580db630266e792fb24769a08a1c0a752e4707#diff



